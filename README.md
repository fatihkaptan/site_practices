directory tree:
```
demo_Sitem:.
├──Html5
│   │   01-hello-world.html
│   │   02-my_first_page.html
│   │   03-header-tags.html
│   │   04-paragraf-etiketi.html
│   │   05-bicimlendirme-etiketeri.html
│   │   06-sirasiz-listeler.html
│   │   07-sirali-listeler.html
│   │   08-resimler.html
│   │   09-formlar.html
│   │   10-tablolar.html
│   │   11-linkler.html
│   │   12-div-span.html
│   │   13-semantic-elements.html
│   │   14-iframe.html
│   │
│   ├───img
│   └───uygulamalar
│       │   uyg2.html
│       │   uyg5.html
│       └───dizin_example
│
│
│       
├───CSS
│   ├───01-Internal-Inline-Css
│   ├───02-External-Css
│   ├───03-IDs-Classes
│   ├───04-Group-Selectors
│   ├───05-Attribute-Selectors
│   ├───06-Pseudo-Selectors
│   ├───07-CSS-Inherit
│   ├───08-Divs
│   ├───09- Floating-Images
│   ├───10-Floating-Divs
│   ├───11-Display
│   ├───12-Positioning
│   ├───13-Margin
│   ├───14-Padding
│   ├───15-Borders
│   ├───16-BoxSizing-BoxModel
│   ├───17-Fonts
│   ├───18-Icons
│   ├───19-Text-Format
│   ├───20-Aligning-Text
│   ├───21-List-Format
│   ├───22-Link-Format
│   └───Uygulamalar
│       ├───Uyg00-ToDo-List
│       ├───Uyg01-Class-IDs
│       ├───Uyg02-Selectors
│       ├───Uyg03-Styling-Table
│       ├───Uyg04-Divs
│       ├───Uyg05-Floating
│       ├───Uyg06-Display
│       ├───Uyg07-Positioning
│       ├───Uyg08-Positioning2
│       ├───Uyg09-BoxModel
│       └───Uyg10-Text
│       ├───Uyg11-List-Buton
│       ├───Uyg12-Box-Shadow
│       ├───Uyg13-Transitions
│       ├───Uyg14-Cards
│       ├───Uyg15-BoxShadow&Transitions
│       ├───Uyg16- Navbar
│       └───Uyg17- Gallery

        
```

